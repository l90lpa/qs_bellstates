﻿using System;

using Microsoft.Quantum.Simulation.Core;
using Microsoft.Quantum.Simulation.Simulators;

namespace Bell
{
    class Driver
    {
        static string arg_bell = "bell";
        static string arg_superposition = "superposition";

        static void bell() {
            using (var qsim = new QuantumSimulator())
            {
                // Try initial values
                Result[] initials = new Result[] { Result.Zero, Result.One };
                foreach (Result initial in initials)
                {
                    var res = BellTest.Run(qsim, 1000, initial).Result;
                    var (numZeros, numOnes, agree) = res;
                    System.Console.WriteLine(
                        $"Init:{initial,-4} 0s={numZeros,-4} 1s={numOnes,-4} agree={agree, -4}");
                }
            }
        }

        static void superposition() {
            using (var qsim = new QuantumSimulator())
            {
                // Try initial values
                Result[] initials = new Result[] { Result.Zero, Result.One };
                foreach (Result initial in initials)
                {
                    var res = SuperpositionTest.Run(qsim, 1000, initial).Result;
                    var (numZeros, numOnes) = res;
                    System.Console.WriteLine(
                        $"Init:{initial,-4} 0s={numZeros,-4} 1s={numOnes,-4}");
                }
            }
        }

        static int checkArgs(string[] args) {
            if(args.Length == 0) {
                System.Console.WriteLine("Please specify the test to run: 'bell' or 'superposition'.");
                return 1;
            }

            if(args[0] != arg_bell && args[0] != arg_superposition) {
                System.Console.WriteLine("An incorrect test was specified, ");
                System.Console.WriteLine(args[0]);
                System.Console.WriteLine(", please select: 'bell' or 'superposition'.");
                return 2;
            }

            return 0;
        }

        static int Main(string[] args)
        {
            int err = checkArgs(args);
            if(err != 0) {
                return err;
            }

            if(args[0] == arg_bell) {
                bell();
            } else {
                superposition();
            }

            System.Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            return 0;
        }
    }
}