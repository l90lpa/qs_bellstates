
## Bell

This repo contains Q# kernels for running tests for varifying assumptions wrt to entangled particles (Bell states) and superposition of a particle.

## To build/run

- In a terminal set the working directory to the root level directory of this project.
- run `dotnet run INSERT_ARGUMENT`, where the available arguments are `bell` or `superposition`.