﻿namespace Bell
{
    open Microsoft.Quantum.Canon;
    open Microsoft.Quantum.Intrinsic;

    operation Put(desired: Result, q1: Qubit) : Unit {
        if (desired != M(q1)) {
            X(q1);
        }
    }

    operation SuperpositionTest (count : Int, initial: Result) : (Int, Int) {

        mutable numOnes = 0;
        using (qubit = Qubit()) {

            for (test in 1..count) {
                Put (initial, qubit);
                //X(qubit); // <--- adding this in perfroms a flip of the state
                H(qubit); // <--- adding this in brings the state into a superposition that is not strictly the state 0> or 1>
                let res = M (qubit);

                // Count the number of ones we saw:
                if (res == One) {
                    set numOnes += 1;
                }
            }
            Put(Zero, qubit);
        }

        // Return number of times we saw a |0> and number of times we saw a |1>
        return (count-numOnes, numOnes);
    }

    operation BellTest (count : Int, initial: Result) : (Int, Int, Int) {

        mutable numOnes = 0;
        mutable agree = 0;

        using ((q0, q1) = (Qubit(), Qubit())) { // could also use `qubits = Qubit[2]` to allocate an array of 2 qubits

            for (test in 1..count) {
                Put (initial, q0);
                Put (Zero, q1);

                H(q0);
                CNOT(q0, q1);
                let res = M (q0);

                if (M (q1) == res) {
                    set agree += 1;
                }

                // Count the number of ones we saw:
                if (res == One) {
                    set numOnes += 1;
                }
            }
            Put(Zero, q0);
            Put(Zero, q1);
        }

        // Return number of times we saw a |0> and number of times we saw a |1>
        return (count-numOnes, numOnes, agree);
    }
}
